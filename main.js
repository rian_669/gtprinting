//Mobile
const mobileNav = document.querySelector(".mobile-nav");
const menuIcon = document.querySelector(".menu-icon");
const mobileMenuItems = document.querySelectorAll(".mobile-nav .menu-items li");

menuIcon.addEventListener("click", () => {
    mobileNav.classList.toggle("active");
});

mobileMenuItems.forEach((i) => {
    i.addEventListener("click", () => {
        mobileNav.classList.remove("active");
    });
});

// Desktop
const options = {
    threshold: 0.8,
};
const addActiveClass = (entries, observer) => {
    entries.forEach((entry) => {
        if(entry.isIntersecting && entry.intersectionRatio >= 0.2){
            //console.log(entry.target);
            let currentActive = document.querySelector(".desktop-nav-edited a.active");

            if(currentActive) {
                currentActive.classList.remove("active")
            }
            let newActive = document.querySelector(
                `.desktop-nav-edited a[href="#${entry.target.getAttribute("id")}"]`
            );
            newActive.classList.add("active");
        }
    });
};

const observer = new IntersectionObserver(addActiveClass,  options);
const section = document.querySelectorAll("section");
section.forEach((section)=> {
    observer.observe(section);
});

// Data Table

const hargaA1 = 1;
const hargaA2 = 2;
const hargaB1 = 3;
const hargaB2 = 4;
const hargaC1 = 5;
const hargaC2 = 10;

const kolomjumlahA1 = document.querySelector(".kolom-jumlah-A1");
const kolomjumlahA2 = document.querySelector(".kolom-jumlah-A2");
const kolomjumlahB1 = document.querySelector(".kolom-jumlah-B1");
const kolomjumlahB2 = document.querySelector(".kolom-jumlah-B2");
const kolomjumlahC1 = document.querySelector(".kolom-jumlah-C1");
const kolomjumlahC2 = document.querySelector(".kolom-jumlah-C2");

// Slider 

const rangeSlider = document.querySelector("input");
const value = document.querySelector(".value");
const valueOnText = document.querySelector(".valueOnText");
value.textContent = rangeSlider.value;

rangeSlider.oninput = function(){
    value.textContent = "Rp "+this.value+".000";
    valueOnText.textContent = value.textContent;
    kolomjumlahA1.textContent = Math.floor(this.value/hargaA1);
    kolomjumlahA2.textContent = Math.floor(this.value/hargaA2);
    kolomjumlahB1.textContent = Math.floor(this.value/hargaB1);
    kolomjumlahB2.textContent = Math.floor(this.value/hargaB2);
    kolomjumlahC1.textContent = Math.floor(this.value/hargaC1);
    kolomjumlahC2.textContent = Math.floor(this.value/hargaC2);
};